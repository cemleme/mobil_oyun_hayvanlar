package games.hebele.hayvanlari_ogreniyorum.Views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FitViewport;

public class AbstractScreen implements Screen {
	
	public float Virtual_Width=480;
	public float Virtual_Height=320;
	private FitViewport viewport;
	public SpriteBatch spriteBatch;
	public Stage stage;
	private OrthographicCamera cam;
	
	public AbstractScreen(){
		spriteBatch = new SpriteBatch();
		viewport = new FitViewport(Virtual_Width, Virtual_Height);
		stage = new Stage(viewport, spriteBatch);
		cam = new OrthographicCamera();
	}
	
	@Override
	public void render(float delta) {
    	Gdx.gl.glClearColor(0 / 255.0f, 0 / 255.0f, 0 / 255.0f, 1); 
 		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
 		spriteBatch.setProjectionMatrix(cam.combined);
	}

	@Override
	public void show() {

		viewport = new FitViewport(Virtual_Width, Virtual_Height);
		stage = new Stage(viewport, spriteBatch);
		cam = new OrthographicCamera();
		cam.setToOrtho(false, Virtual_Width, Virtual_Height);
		
        InputProcessor backProcessor = new InputAdapter() {
            @Override
            public boolean keyDown(int keycode) {
            	
                if ((keycode == Keys.BACKSPACE) || (keycode == Keys.BACK)){
                	goToPreviousScreen();
                }
                return false;
            }
        };

        InputMultiplexer multiplexer = new InputMultiplexer(backProcessor,stage);
        Gdx.input.setInputProcessor(multiplexer);
	}
	
	public void goToPreviousScreen(){}

	@Override
	public void dispose() {
        spriteBatch.dispose();
        stage.dispose();
	}
	
	@Override
	public void resize(int width, int height) {}

	@Override
	public void hide() {}

	@Override
	public void pause() {}

	@Override
	public void resume() {}
}
