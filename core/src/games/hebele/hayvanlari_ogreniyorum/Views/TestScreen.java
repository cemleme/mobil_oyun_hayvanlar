package games.hebele.hayvanlari_ogreniyorum.Views;

import games.hebele.hayvanlari_ogreniyorum.Assets;
import games.hebele.hayvanlari_ogreniyorum.HayvanlariOgreniyorum;
import games.hebele.hayvanlari_ogreniyorum.Controllers.GameWorld;
import games.hebele.hayvanlari_ogreniyorum.Managers.LevelManager;
import games.hebele.hayvanlari_ogreniyorum.Managers.SoundManager;
import games.hebele.hayvanlari_ogreniyorum.Models.animalActor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

public class TestScreen extends AbstractScreen {
	 
	
	HayvanlariOgreniyorum game; 
	private GameWorld myWorld;
	private Group hayvanGrubu;

	private String level;

	BitmapFont font;
	
	private TextureRegion levelBackground, textureStar;
	private TextureAtlas guiAtlas;
	
	private float Virtual_Width=480;
	private float Virtual_Height=320;
	private float runTime=0;
	
	public enum TestState {
	    RUNNING,
	    DOGRU,
	    NEXTLEVEL
	}
	
	public TestState currentTestState;
	
	public LevelManager levelManager;

    // constructor to keep a reference to the main Game class
     public TestScreen(HayvanlariOgreniyorum game, GameWorld myWorld){
            this.game = game;
            this.myWorld=myWorld;
            
            currentTestState= TestState.RUNNING;
           
        	levelManager = new LevelManager();
     }
	
	public String getLevelTarget(){
		return level;
	}
     
     @Override
     public void render(float delta) {
    	super.render(delta);

    	spriteBatch.begin();
    	spriteBatch.setColor(1f, 1f, 1f, 1f);
    	spriteBatch.draw(levelBackground,0,0,Virtual_Width,Virtual_Height);
        
    	spriteBatch.end();
        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();
        

        TestStateUpdate(); 
        
        runTime+=delta;
        if(runTime>1) runTime=0f;
     }

     
 	private void TestStateUpdate() {
	     // handle update
	    switch(currentTestState) {
	       case RUNNING:
	    	  
	          break;
	       case DOGRU:
	    	  drawFallingStars();
	    	  Timer.schedule(new Task(){
	    		   @Override
	    		   public void run(){
	    			   currentTestState = TestState.NEXTLEVEL;
	    		      }
	    		   }, 2.0f);
	    	  /*
              Thread t = new Thread(new Runnable() {
                  public void run()
                  {
          	    	try {
      					Thread.sleep(2000);
      				} catch (InterruptedException e) {
      					e.printStackTrace();
      				}
          	    	currentTestState = TestState.NEXTLEVEL;
                  }
             });
             t.start();  
             */
             break;
	       case NEXTLEVEL:
	    	   levelManager.goToNextlevel(); 
	    	   break;
	    }
	}
 	
 	private void drawFallingStars(){
 		spriteBatch.begin();
 		spriteBatch.setColor(1f, 1f, 1f, (1-(runTime/3f)));
 		drawSingleFallingStar(1/11f, 20, 1.7f);
 		drawSingleFallingStar(2/11f, 50, 1.8f);
 		drawSingleFallingStar(3/11f, 10, 2.4f);
 		drawSingleFallingStar(4/11f, 30, 1.9f);
 		drawSingleFallingStar(5/11f, 50, 1.4f);
 		drawSingleFallingStar(6/11f, 30, 1.7f);
 		drawSingleFallingStar(7/11f, 10, 2.5f);
 		drawSingleFallingStar(8/11f, 20, 1.8f);
 		drawSingleFallingStar(9/11f, 50, 1.6f);
 		drawSingleFallingStar(10/11f, 30, 2.4f);
 		spriteBatch.end();
 	}
 	
 	private void drawSingleFallingStar(float xRatio, float starSize, float speed){
 		spriteBatch.draw(textureStar,Virtual_Width*xRatio - starSize/2, Virtual_Height*(1-runTime*speed),starSize,starSize);
 		spriteBatch.draw(textureStar,Virtual_Width*xRatio - starSize/2, Virtual_Height*(runTime*speed),starSize,starSize);
 	}

 	@Override
 	public void goToPreviousScreen(){
 		game.goToMainMenuScreen();
 	}

    @Override
     public void show() {
    	
    	super.show();
    	
    	//INIT LEVEL MANAGER - hayvanlarý yarat, gruba yerlestir
    	initLevel();
    	
		//LOAD ASSETS-----------------------------
		guiAtlas = Assets.manager.get(Assets.guiPack, TextureAtlas.class);
		textureStar = guiAtlas.findRegion("star_full");
    	levelBackground=myWorld.getlevelBackground();


        
	    font = new BitmapFont(Gdx.files.internal("skin/maiandra.fnt"), false);

	   	
	   	
	   	ImageButtonStyle imgBtnStyle = new ImageButtonStyle();
        imgBtnStyle.up= new TextureRegionDrawable(myWorld.button_sound);
        
        ImageButton buttonRepeatSound = new ImageButton(imgBtnStyle);
        buttonRepeatSound.setPosition(20, Virtual_Height-40);
        buttonRepeatSound.setWidth(50);
        buttonRepeatSound.setHeight(36);
	   	
        LabelStyle labelStyle = new LabelStyle(font,Color.WHITE);
        Label hayvanAdi = new Label(getLevelTarget(),labelStyle);
        hayvanAdi.setPosition(20, Virtual_Height-90);
	   	
       stage.addActor(hayvanGrubu);
       stage.addActor(buttonRepeatSound);
       stage.addActor(hayvanAdi);
       
	   SoundManager.playSoundHangisi(level);
       
       
       hayvanGrubu.addListener(new InputListener() {
    	   @Override
	    	public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
    		   animalActor hayvan = (animalActor) event.getTarget();
               if(hayvan.getName().equals(level)){
            	   SoundManager.playSoundDogru();
            	   hayvan.dogruCevapAksiyonlari();
     	    	   runTime=0;
       	    	   currentTestState = TestState.DOGRU;
               }
               else{
                   SoundManager.playSoundYanlisHayvan(hayvan.getName());
            	   hayvan.yanlisCevapAksiyonlari();
               }
               return true;
	    	}
       });
            
       
       buttonRepeatSound.addListener(new InputListener() {
    	   @Override
	    	public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
       	   		SoundManager.playSoundHangisi(level);
      			return true;
	    	}
       });
       
    }
    
    public void initLevel(){
    	hayvanGrubu = levelManager.hayvanlariYaratYerlestir();
    	level = levelManager.getCurrentLevelName();
    }
}