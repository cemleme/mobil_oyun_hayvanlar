package games.hebele.hayvanlari_ogreniyorum.Managers;

import games.hebele.hayvanlari_ogreniyorum.HayvanlariOgreniyorum;
import games.hebele.hayvanlari_ogreniyorum.Models.HayvanFactory;
import games.hebele.hayvanlari_ogreniyorum.Models.animalActor;

import java.util.ArrayList;
import java.util.Random;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;

public class LevelManager {

	private final int NUM_ANIMALS = 3;
	
	public static HayvanlariOgreniyorum game; 
	
	private int TOTAL_NUM_ANIMALS;
	private int currentLevelID;
	private String levelName;
	public static int lastLevelID;
	
	private TextureRegion[] secilenHayvanlar = new TextureRegion[NUM_ANIMALS];
	private String[] secilenHayvanAdlari = new String[NUM_ANIMALS];
	private int[] secilenHayvanIdleri = new int[NUM_ANIMALS];
	

	private  Vector2 POSITIONS[]= {
						new Vector2(5,60),
						new Vector2(190,10),
						new Vector2(310,30),
						new Vector2(40,170),
						new Vector2(320,220) };
	
	
	public static GameState currentState;
	public static boolean isRandomMode;
	
	public static enum GameState {
        KARA, SU
    }
	
	public static void init(){
		currentState=GameState.KARA;
		isRandomMode=false;
		lastLevelID=500;
	}
	
    private void RandomHayvanSec(){
    	
    	ArrayList<Integer> numbers = new ArrayList<Integer>();  

	   	while (numbers.size() < NUM_ANIMALS) {

	   	    int random = new Random().nextInt(TOTAL_NUM_ANIMALS);
			if (!numbers.contains(random) && random!=lastLevelID) {
	   	        numbers.add(random);
	   	    }
	   	}
	   	
    	for(int i=0;i<NUM_ANIMALS;i++){
    		secilenHayvanlar[i]=HayvanFactory.getAnimalTexture(numbers.get(i));
    	   	secilenHayvanIdleri[i]=numbers.get(i);
    	   	secilenHayvanAdlari[i]=HayvanFactory.getAnimalName(numbers.get(i));
    	   	
        	if(secilenHayvanlar[i].isFlipX()){
        		secilenHayvanlar[i].flip(true, false);
        	}
    	}
   	
    	//ILK HAYVAN SAGA DOGRU BAKSIN
    	if(!secilenHayvanlar[0].isFlipX()){
    		secilenHayvanlar[0].flip(true, false);
    	}
    	
    	//EN SAGDAKI HAYVANI SAGA YAKLASTIR
    	POSITIONS[1].x=POSITIONS[0].x+secilenHayvanlar[0].getRegionWidth()+5;
    	POSITIONS[2].x=480-secilenHayvanlar[2].getRegionWidth()-5;
    	
    	float pos1=POSITIONS[1].x;
    	float size1=secilenHayvanlar[1].getRegionWidth();
    	float pos2=POSITIONS[2].x;
    	float diff=pos2-size1-pos1;
    	if(diff>0) POSITIONS[1].x+=diff/2;
    	
	   	//HEDEF HAYVAN SEC
    	int i = new Random().nextInt(NUM_ANIMALS);
    	currentLevelID = secilenHayvanIdleri[i];
    	levelName=secilenHayvanAdlari[i];
    }
    
    public Group hayvanlariYaratYerlestir(){
    	TOTAL_NUM_ANIMALS = HayvanFactory.getTotalNumOfAnimals();
    	RandomHayvanSec();
    	Group hayvanGrubu = new Group();
    	
    	for(int i=0; i<NUM_ANIMALS; i++){  		
    		animalActor hayvan = new animalActor(secilenHayvanlar[i], POSITIONS[i], secilenHayvanAdlari[i]);
    		hayvan.setOriginX(hayvan.getWidth()/2);
    		hayvanGrubu.addActor(hayvan);
    	}
    	
    	return hayvanGrubu;
    }
    
    public int getCurrentLevelID(){
    	return currentLevelID;
    }
    
    public String getCurrentLevelName(){
    	return levelName;
    }
    
    public void goToNextlevel(){
		if(isRandomMode) setRandomGameTheme();
		game.newTestScreen(getCurrentLevelID());
	}
    
	public static void setRandomGameTheme(){
		if(new Random().nextInt(2)==1)
			currentState=GameState.KARA;
		else 
			currentState=GameState.SU;
	}
	
	public boolean isRandomMode(){
		return isRandomMode;
	}
	
	public static void setRandomMode(boolean mode){
		isRandomMode=mode;
	}

	public static void setGameTheme(GameState state) {
		currentState=state;
	}
	
	public static void setKaraTheme(){
		currentState = GameState.KARA;
	}
	
	public static void setSuTheme(){
		currentState = GameState.SU;
	}
}
