package games.hebele.hayvanlari_ogreniyorum.Models;

import games.hebele.hayvanlari_ogreniyorum.Managers.LevelManager;
import games.hebele.hayvanlari_ogreniyorum.Managers.SoundManager;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class HayvanFactory {
	public ArrayList<HayvanPrototype> hayvanlar;
	
	public static ArrayList<TextureRegion> hayvanKaraTextures;  
	public static ArrayList<String> hayvanKaraNames;  

	public static ArrayList<TextureRegion> hayvanSuTextures;  
	public static ArrayList<String> hayvanSuNames;  
	
	public static void init(){
		hayvanKaraTextures = new ArrayList<TextureRegion>();  
		hayvanKaraNames = new ArrayList<String>();  
		hayvanSuTextures = new ArrayList<TextureRegion>();  
		hayvanSuNames = new ArrayList<String>();  
	}
	
	public void processData(TextureAtlas hayvanAtlas){
		for (int i = 0; i < hayvanlar.size(); i++){
			String ad = hayvanlar.get(i).ad;
			String ad_lower = ad.toLowerCase().replaceAll("\\s","");
			String nick = hayvanlar.get(i).nickENG;
			if(nick==null) nick = ad_lower; 
			String tip = hayvanlar.get(i).tip;
			
			SoundManager.soundMap.put("sound_"+ad_lower, Gdx.audio.newSound(Gdx.files.internal("sound/"+nick+".mp3")));
			
			if(tip.equals("KARA")){
				hayvanKaraTextures.add(hayvanAtlas.findRegion(nick));
				hayvanKaraNames.add(ad);
			}else{
				hayvanSuTextures.add(hayvanAtlas.findRegion(nick));
				hayvanSuNames.add(ad);
			}
		}
	}
	
	public static TextureRegion getAnimalTexture(int index){
        switch (LevelManager.currentState) {
        case KARA:
        default:
    		return hayvanKaraTextures.get(index);
		case SU:
			return hayvanSuTextures.get(index);
        }
	}
	
	public static String getAnimalName(int index){
        switch (LevelManager.currentState) {
        case KARA:
        default:
    		return hayvanKaraNames.get(index);
		case SU:
			return hayvanSuNames.get(index);
        }
	}
	
	public static int getTotalNumOfAnimals(){
		
        switch (LevelManager.currentState) {
        case KARA:
        default:
        	return hayvanKaraNames.size();
		case SU:
        	return hayvanSuNames.size();
        }
	}
}
