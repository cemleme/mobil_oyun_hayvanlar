package games.hebele.hayvanlari_ogreniyorum.Views;

import java.util.Random;

import games.hebele.hayvanlari_ogreniyorum.HayvanlariOgreniyorum;
import games.hebele.hayvanlari_ogreniyorum.Controllers.GameWorld;
import games.hebele.hayvanlari_ogreniyorum.Managers.LevelManager;
import games.hebele.hayvanlari_ogreniyorum.Models.HayvanFactory;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class MainMenuScreen extends AbstractScreen {

	HayvanlariOgreniyorum myGame;
	GameWorld myWorld;
	

	private ImageButton buttonKara, buttonSu, buttonRandom;
    
    private TextureRegion hayvan1,hayvan2,hayvan3,hayvan4,hayvan5,bg,oyun_logo;
    private float rotation,rotationSpeed;
    private float w1,h1,w2,h2,w3,h3,w4,h4,w5,h5;
    

	// constructor to keep a reference to the main Game class
	public MainMenuScreen(HayvanlariOgreniyorum game, GameWorld world) {
		this.myGame = game;
		this.myWorld=world;
		rotation=0;
		rotationSpeed=30;
		

		oyun_logo=myWorld.oyun_logo;
		bg= new TextureRegion(myWorld.bg_landscape);
		
		
        float buttonWidth=Virtual_Width*0.4f;
        float buttonHeight=buttonWidth*myWorld.btn_tum_up.getRegionHeight()/myWorld.btn_tum_up.getRegionWidth();
        float buttonGap=-5;
        float buttonMarginTop=15;
        
        ImageButtonStyle imgBtnStyle = new ImageButtonStyle();
        imgBtnStyle.up= new TextureRegionDrawable(myWorld.btn_tum_up);
        imgBtnStyle.down= new TextureRegionDrawable(myWorld.btn_tum_down);
        
        buttonRandom = new ImageButton(imgBtnStyle);
        buttonRandom.setPosition(20,Virtual_Height-buttonMarginTop-buttonHeight);
        buttonRandom.setWidth(buttonWidth);
        buttonRandom.setHeight(buttonHeight);
        
        
        ImageButtonStyle imgBtnStyleKara = new ImageButtonStyle();
        imgBtnStyleKara.up= new TextureRegionDrawable(myWorld.btn_kara_up);
        imgBtnStyleKara.down= new TextureRegionDrawable(myWorld.btn_kara_down);
        
        buttonKara = new ImageButton(imgBtnStyleKara);
        buttonKara.setPosition(20, Virtual_Height-buttonMarginTop-buttonGap-buttonHeight*2);
        buttonKara.setWidth(buttonWidth);
        buttonKara.setHeight(buttonHeight);
        
        
        ImageButtonStyle imgBtnStyleSu = new ImageButtonStyle();
        imgBtnStyleSu.up= new TextureRegionDrawable(myWorld.btn_su_up);
        imgBtnStyleSu.down= new TextureRegionDrawable(myWorld.btn_su_down);
        
        buttonSu = new ImageButton(imgBtnStyleSu);
        buttonSu.setPosition(20, Virtual_Height-buttonMarginTop-buttonGap*2-buttonHeight*3);
        buttonSu.setWidth(buttonWidth);
        buttonSu.setHeight(buttonHeight);
        
        
        buttonRandom.addListener(new ChangeListener() {
            public void changed (ChangeEvent event, Actor actor) {
            	LevelManager.setRandomMode(true);
            	LevelManager.setRandomGameTheme();	
            	myGame.newTestScreen(500);
            }
        });
        
        
        buttonKara.addListener(new ChangeListener() {
            public void changed (ChangeEvent event, Actor actor) {
            	LevelManager.setRandomMode(false);
            	LevelManager.setKaraTheme();	
            	myGame.newTestScreen(500);
            }
        });     
        
        buttonSu.addListener(new ChangeListener() {
            public void changed (ChangeEvent event, Actor actor) {
            	LevelManager.setRandomMode(false);
            	LevelManager.setSuTheme();
            	myGame.newTestScreen(500);
            }
        });
        
	}

	@Override
	public void render(float delta) {
		super.render(delta);
		stage.act(delta);
		
		rotation+=rotationSpeed*delta;
		
		if(rotation<-10){ 
			rotation=-10;
			rotationSpeed*=-1; 
		}
		if(rotation>10){  
			rotation=10;
			rotationSpeed*=-1;
		}
		
        spriteBatch.begin();
        spriteBatch.draw(bg,0,0,Virtual_Width,Virtual_Height);
        spriteBatch.draw(oyun_logo,250,175,130,130*oyun_logo.getRegionHeight()/oyun_logo.getRegionWidth());

        
        spriteBatch.draw(hayvan5,350, 10, w5/2, 0, w5, h5, 0.6f, 0.6f, -rotation);
        spriteBatch.draw(hayvan4,260, 15, w4/2, 0, w4, h4, 0.6f, 0.6f,rotation+2);
        spriteBatch.draw(hayvan3,190, 20, w3/2, 0, w3, h3,  0.6f, 0.6f,-rotation);
        spriteBatch.draw(hayvan2,110, 15, w2/2, 0, w2, h2,  0.6f, 0.6f, rotation+1);
        spriteBatch.draw(hayvan1,10, 10, w1/2, 0, w1, h1,  0.6f, 0.6f, -rotation-1);

        
        spriteBatch.end();
		
		stage.draw();
	}


	@Override
	public void show() {
		super.show();
		
		int numAnimals=HayvanFactory.getTotalNumOfAnimals();
		
		int i1=new Random().nextInt(numAnimals);
		int i2=new Random().nextInt(numAnimals);
		while(i1==i2) i2=new Random().nextInt(numAnimals);

		int i3=new Random().nextInt(numAnimals);
		while(i1==i3 || i2==i3) i3=new Random().nextInt(numAnimals);

		int i4=new Random().nextInt(numAnimals);
		while(i1==i4 || i2==i4 || i3==i4) i4=new Random().nextInt(numAnimals);	

		int i5=new Random().nextInt(numAnimals);
		while(i1==i5 || i2==i5 || i3==i5 || i4==i5) i5=new Random().nextInt(numAnimals);
		
		
		hayvan1=new TextureRegion(HayvanFactory.getAnimalTexture(i1));
		hayvan1.flip(true, false);
		hayvan2=new TextureRegion(HayvanFactory.getAnimalTexture(i2));
		hayvan2.flip(false, false);
		hayvan3=new TextureRegion(HayvanFactory.getAnimalTexture(i3));
		hayvan3.flip(false, false);
		hayvan4=new TextureRegion(HayvanFactory.getAnimalTexture(i4));
		hayvan4.flip(false, false);
		hayvan5=new TextureRegion(HayvanFactory.getAnimalTexture(i5));
		hayvan5.flip(false, false);
/*
		float animalScale=0.6f;	
		 
		 w1=animalScale*hayvan1.getRegionWidth()*Gdx.graphics.getWidth()/480;
		 h1=w1*hayvan1.getRegionHeight()/hayvan1.getRegionWidth();	
		 
		 w2=animalScale*hayvan2.getRegionWidth()*Gdx.graphics.getWidth()/480;
		 h2=w2*hayvan2.getRegionHeight()/hayvan2.getRegionWidth();	
		 
		 w3=animalScale*hayvan3.getRegionWidth()*Gdx.graphics.getWidth()/480;
		 h3=w3*hayvan3.getRegionHeight()/hayvan3.getRegionWidth();	
		 
		 w4=animalScale*hayvan4.getRegionWidth()*Gdx.graphics.getWidth()/480;
		 h4=w4*hayvan4.getRegionHeight()/hayvan4.getRegionWidth();
		 
		 w5=animalScale*hayvan5.getRegionWidth()*Gdx.graphics.getWidth()/480;
		 h5=w5*hayvan5.getRegionHeight()/hayvan5.getRegionWidth();
        */
		w1=hayvan1.getRegionWidth();
		w2=hayvan2.getRegionWidth();
		w3=hayvan3.getRegionWidth();
		w4=hayvan4.getRegionWidth();
		w5=hayvan5.getRegionWidth();
		
		h1=hayvan1.getRegionHeight();
		h2=hayvan2.getRegionHeight();
		h3=hayvan3.getRegionHeight();
		h4=hayvan4.getRegionHeight();
		h5=hayvan5.getRegionHeight();
		
		
		/*
        buttonsAtlas = new TextureAtlas("skin/buttons.pack");    
    
        NinePatchDrawable btnNormal9Drawable = new NinePatchDrawable(buttonsAtlas.createPatch("btn_default_normal_green"));
        NinePatchDrawable btnPressedDrawable = new NinePatchDrawable(buttonsAtlas.createPatch("btn_default_pressed"));
        
        TextButtonStyle style = new TextButtonStyle(btnNormal9Drawable, btnPressedDrawable, btnNormal9Drawable, new BitmapFont());
      
        buttonStartGame = new TextButton("OYNA", style);
        
        buttonStartGame.getStyle().font.setScale(screenRatio);
         
        buttonStartGame.setWidth(Gdx.graphics.getWidth()/4);
        buttonStartGame.setHeight(Gdx.graphics.getHeight()/8);
        buttonStartGame.setPosition(40, 40);
        */
        

        //stage.addActor(buttonStartGame);
        stage.addActor(buttonRandom);
        stage.addActor(buttonKara);
        stage.addActor(buttonSu);
        
	}
	

 	@Override
 	public void goToPreviousScreen(){
 		Gdx.app.exit();
 	}

}