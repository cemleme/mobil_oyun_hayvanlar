package games.hebele.hayvanlari_ogreniyorum;

import games.hebele.hayvanlari_ogreniyorum.Controllers.GameWorld;
import games.hebele.hayvanlari_ogreniyorum.Managers.LevelManager;
import games.hebele.hayvanlari_ogreniyorum.Managers.SoundManager;
import games.hebele.hayvanlari_ogreniyorum.Models.HayvanFactory;
import games.hebele.hayvanlari_ogreniyorum.Views.MainMenuScreen;
import games.hebele.hayvanlari_ogreniyorum.Views.SplashScreen;
import games.hebele.hayvanlari_ogreniyorum.Views.TestScreen;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;

public class HayvanlariOgreniyorum extends Game {
	private GameWorld myWorld;
    
    
    @Override
    public void create() {  
    	initStatics();
    	 	
    	Assets.load();
		Assets.manager.finishLoading();

		Gdx.input.setCatchBackKey(true);
        
        myWorld=new GameWorld();
        
        setScreen(new SplashScreen(this));  
    }
    
    public void initStatics(){
    	Assets.init();
    	HayvanFactory.init();
    	LevelManager.init();
    	LevelManager.game = this;
    	SoundManager.init();
    }
    
    public void goToMainMenuScreen(){
    	setScreen(new MainMenuScreen(this,myWorld));  
    }
    
    public void newTestScreen(int lastLevelID){
    	LevelManager.lastLevelID = lastLevelID;
    	setScreen(new TestScreen(this, myWorld));  
    }
    
    @Override
    public void dispose(){
    	super.dispose();
    	Assets.dispose();
    	myWorld.dispose();
    }

}
